import {combineReducers, configureStore} from '@reduxjs/toolkit';
import messageReducer from './message';
import usersReducer from './users';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import thunk from 'redux-thunk';
const reducers = combineReducers({
  message: messageReducer,
  users: usersReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk],
});
