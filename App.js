import {StyleSheet, View} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import {store} from './store';
import {persistStore} from 'redux-persist';
import {PersistGate} from 'redux-persist/es/integration/react';
import Users from './src/components/Users';
import Message from './src/components/Message';

const persistedStore = persistStore(store);
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistedStore}>
        <View style={styles.container}>
          {/* <Message /> */}
          <Users />
        </View>
      </PersistGate>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 4,
    backgroundColor: 'dodgerblue',
    marginTop: 40,
  },
});
