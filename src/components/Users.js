import {
  StyleSheet,
  Text,
  View,
  Pressable,
  ActivityIndicator,
} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

import {fetchUsers, selectAllUsers} from '../../store/users';
const Users = () => {
  const dispatch = useDispatch();
  const {loading} = useSelector(state => state.users);
  const users = useSelector(selectAllUsers);

  const handlePress = () => {
    dispatch(fetchUsers());
  };

  if (loading) {
    return <ActivityIndicator size="large" style={styles.loader} />;
  }
  return (
    <View style={styles.content}>
      {users.map(user => {
        return (
          <View style={styles.container} key={user.id}>
            <View>
              <View style={styles.dataContainer}>
                <Text>{user.name}</Text>
              </View>
              <View style={styles.dataContainer}>
                <Text>{user.email}</Text>
              </View>
            </View>
          </View>
        );
      })}
      <Pressable onPress={() => handlePress()} style={styles.button}>
        <Text>Press Me</Text>
      </Pressable>
    </View>
  );
};

export default Users;

const styles = StyleSheet.create({
  content: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 4,
    backgroundColor: 'dodgerblue',
    marginTop: 40,
  },
  loader: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  container: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  dataContainer: {
    flexDirection: 'row',
  },
});
