import {StyleSheet, Text, View, Pressable} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setMessage} from '../../store/message';

const Message = () => {
  const dispatch = useDispatch();
  const {message} = useSelector(state => state.message);

  const handlePress = () => {
    dispatch(setMessage('Hello world!'));
  };

  return (
    <View style={styles.content}>
      <Text>{message}</Text>
      <Pressable onPress={() => handlePress()} style={styles.button}>
        <Text>Press Me</Text>
      </Pressable>
    </View>
  );
};

export default Message;

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
  },
  button: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 4,
    backgroundColor: 'dodgerblue',
    marginTop: 40,
  },
});
